# README #
Assignment Instructions

### Guidelines ###
* All programming should be done in C# or Bolt
* All UI elements should be done using Unity inbuilt UI.
* You need to create one scene only, where we can navigate all steps through next-back icons/buttons

#### Exercise 1 ####
Create 3d drag & drop & feedback

* Create Four objects (cube01,sphere02,cube03,cylinder04) in the scene view
* If the user drags cube01 to cylinder04 then cube01 & cylinder04 shud both get invisible; Also cube03 & UI panel should now be visible with the text  “Correct answer”
* Else if the user drags cube01 anywhere else than cylinder04, then cube01 shud return to its original position & UI panel shud be visible with the text “Incorrect answer”

#### Exercise 2 ####
Make a basic Multiple Choice Question with correct and incorrect states functioning on 4 options

#### Exercise 3 ####
Create one slider to control deflection (object name - Dummy_pointer) in the Meter reading (fbx link)

#### Exercise 4 ####
You have to arrange an fbx(fbx link) in the timeline & control its play & pause button. Clicking on Pause, shud change the material (object name - tube01) via c# Turn off GameObject - (box01,sphere01)

#### Optional ####
Make a graph using a given equation: y = mx + c, where m and c are constants.
