﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Exercise1
{
    public class Exercise1UIManager : MonoSingleton<Exercise1UIManager>
    {
        private const string CORRECT = "Correct";
        private const string INCORRECT = "Incorrect";

        private Text currStatusText;

        protected override void InstanceSet()
        {
            currStatusText = GetComponentInChildren<Text>();
            ClearText();
        }

        public void ClearText()
        {
            currStatusText.text = "";
        }

        public void SetCorrectText()
        {
            currStatusText.text = CORRECT;
        }

        public void SetIncorrectText()
        {
            currStatusText.text = INCORRECT;
        }
    }
}