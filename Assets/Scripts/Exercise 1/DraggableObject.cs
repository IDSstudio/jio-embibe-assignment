﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jio.Embibe.Assignment.Exercise1
{
    /// <summary>
    /// Script for making object draggable
    /// </summary>
    public class DraggableObject : MonoBehaviour
    {
        [SerializeField] private GameObject tagetObject;

        private Camera cam;
        private Vector3 defaultPosition;
        private bool isDragging;
        private List<GameObject> currTriggeredObjects;

        private void Awake()
        {
            cam = Camera.main;
            defaultPosition = transform.position;
            currTriggeredObjects = new List<GameObject>();
        }

        private void Update()
        {
            if (isDragging)
            {
                if (Input.GetMouseButton(0))
                {
                    OnDrag();
                }
                else
                {
                    OnEndDrag();
                }
            }
            else if (Input.GetMouseButtonDown(0) && PointOverGameObject())
            {
                // Mouse Button pressed or Touch occured while the pointer was over this gameobject
                OnBeginDrag();
            }
        }

        private void OnBeginDrag()
        {
            isDragging = true;
        }

        private void OnDrag()
        {
            // Since the camera is perspective, Setting z as z diff bw default position and camera to get correct ScreenPos 
            var mouseScreenPos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, defaultPosition.z - cam.transform.position.z);
            var mouseWorldPos = cam.ScreenToWorldPoint(mouseScreenPos);
            transform.position = mouseWorldPos;
        }

        private void OnEndDrag()
        {
            isDragging = false;
            transform.position = defaultPosition;
            if (tagetObject != null && currTriggeredObjects.Contains(tagetObject))
            {
                gameObject.SetActive(false);
                tagetObject.SetActive(false);
                IsCorrectAnswer(true);
            }
            else
            {
                IsCorrectAnswer(false);
            }
        }

        private void IsCorrectAnswer(bool correct)
        {
            if (currTriggeredObjects.Count == 0)
            {
                // There were no triggered objects, so no interactions occured
                Exercise1UIManager.Instance.ClearText();
            }
            else if (correct)
            {
                Exercise1UIManager.Instance.SetCorrectText();
            }
            else
            {
                Exercise1UIManager.Instance.SetIncorrectText();
            }
        }

        /// <summary>
        /// Check if the mouse pointer or touch postion is over this gameobject
        /// </summary>
        /// <returns></returns>
        private bool PointOverGameObject()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                // Ray successfuly hit some gameobject
                if (hit.collider.gameObject == gameObject)
                {
                    return true;
                }
            }
            return false;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (isDragging && !currTriggeredObjects.Contains(other.gameObject))
            {
                // Adding triggered object if dragging and not already present
                currTriggeredObjects.Add(other.gameObject);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            currTriggeredObjects.Remove(other.gameObject);
        }
    }
}