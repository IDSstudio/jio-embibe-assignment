﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Exercise3
{
    public class AmmeterController : MonoBehaviour
    {
        private const string LOC_AMMETER_NEEDLE = "Dummy_Meter/Meter/Dummy003/Ameter_Up Side/Dummy_pointer";

        [SerializeField] private float minRotation = 0;
        [SerializeField] private float maxRotation = 180;

        private Transform needle;
        private Quaternion defaultNeedleRotation;
        private Vector3 needleUpDir;

        private void Awake()
        {
            needle = transform.Find(LOC_AMMETER_NEEDLE);
            defaultNeedleRotation = needle.rotation;
            needleUpDir = needle.up;
        }

        private void Start()
        {
            Exercise3UIManager.Instance.OnAmmeterReadingUpdate += OnAmmeterReadingUpdate;
            Exercise3UIManager.Instance.UpdateAmmeterReadingSlider(0);
        }

        private void OnAmmeterReadingUpdate(float newVal)
        {
            SetNeedle(newVal);
        }

        private void SetNeedle(float value01)
        {
            var rotation = Mathf.Lerp(minRotation, maxRotation, value01);
            // Rotating the default rotation by "rotation" and applying
            needle.rotation = Quaternion.AngleAxis(rotation, needleUpDir) * defaultNeedleRotation;
        }
    }
}