﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Exercise3
{
    public class Exercise3UIManager : MonoSingleton<Exercise3UIManager>
    {
        public event System.Action<float> OnAmmeterReadingUpdate;

        private Slider ammeterSlider;

        protected override void InstanceSet()
        {
            ammeterSlider = GetComponentInChildren<Slider>();
            ammeterSlider.onValueChanged.AddListener((val) => OnAmmeterReadingUpdate?.Invoke(val));
        }

        public void UpdateAmmeterReadingSlider(float val01)
        {
            ammeterSlider.value = val01;
        }
    }
}