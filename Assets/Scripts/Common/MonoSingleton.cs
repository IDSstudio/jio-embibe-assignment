﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inherit this script to easily create singltons.
/// Note: Take Special care of Awake method already implemented here, 
/// Use "InstanceSet" instead for all Awake related handling
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
    public static T Instance { get; protected set; }

    protected abstract void InstanceSet();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = (T)this;
            InstanceSet();
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }
}
