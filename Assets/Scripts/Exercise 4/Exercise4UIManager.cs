﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Exercise4
{
    public class Exercise4UIManager : MonoSingleton<Exercise4UIManager>
    {
        private const string PLAY = "Play";
        private const string PAUSE = "Pause";

        public event System.Action<bool> OnClickPlay;

        private bool isPlaying;
        private Text playPauseText;

        protected override void InstanceSet()
        {
            var playPauseButton = GetComponentInChildren<Button>();
            playPauseText = playPauseButton.GetComponentInChildren<Text>();
            playPauseButton.onClick.AddListener(OnClickPlayPauseButton);
        }

        private void Start()
        {
            OnClickPlayButton();
        }

        private void OnClickPlayPauseButton()
        {
            if (isPlaying)
            {
                OnClickPauseButton();
            }
            else
            {
                OnClickPlayButton();
            }
        }

        private void OnClickPlayButton()
        {
            isPlaying = true;
            playPauseText.text = PAUSE;
            OnClickPlay?.Invoke(true);
        }

        private void OnClickPauseButton()
        {
            isPlaying = false;
            playPauseText.text = PLAY;
            OnClickPlay?.Invoke(false);
        }
    }
}