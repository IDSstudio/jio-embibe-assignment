﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace Jio.Embibe.Assignment.Exercise4
{
    [RequireComponent(typeof(PlayableDirector))]
    public class TimelineController : MonoBehaviour
    {
        [SerializeField] private GameObject[] objectsToEnableDisable;
        [SerializeField] private MaterialSwapper[] objectsToSwapMaterial;
        private PlayableDirector playableDirector;

        private void Awake()
        {
            playableDirector = GetComponent<PlayableDirector>();
        }

        private void Start()
        {
            Exercise4UIManager.Instance.OnClickPlay += PlayTimeline;
        }

        private void PlayTimeline(bool play)
        {
            if (play)
            {
                ResumeTimeline();
            }
            else
            {
                PauseTimeline();
            }
        }

        private void ResumeTimeline()
        {
            SwapMaterials(false);
            EnableObjects(true);
            playableDirector.Resume();
        }

        private void PauseTimeline()
        {
            SwapMaterials(true);
            EnableObjects(false);
            playableDirector.Pause();
        }

        private void SwapMaterials(bool swap)
        {
            for (int i = 0; i < objectsToSwapMaterial.Length; i++)
            {
                objectsToSwapMaterial[i].Swap(swap);
            }
        }

        private void EnableObjects(bool enable)
        {
            for (int i = 0; i < objectsToEnableDisable.Length; i++)
            {
                objectsToEnableDisable[i].SetActive(enable);
            }
        }

        [System.Serializable]
        private class MaterialSwapper
        {
            [SerializeField] private Renderer renderer;
            [SerializeField] private Material alternateMaterial;
            private Material originalMaterial;

            public void Swap(bool swap)
            {
                if (swap)
                {
                    Swap();
                }
                else
                {
                    Restore();
                }
            }

            public void Swap()
            {
                if (originalMaterial == null)
                {
                    originalMaterial = renderer.material;
                }
                renderer.material = alternateMaterial;
            }

            public void Restore()
            {
                if (originalMaterial != null)
                {
                    renderer.material = originalMaterial;
                }
            }
        }
    }
}