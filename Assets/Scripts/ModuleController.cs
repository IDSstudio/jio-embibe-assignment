﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jio.Embibe.Assignment
{
    public class ModuleController : MonoBehaviour
    {
        [SerializeField] private Module[] modules;
        private int currModule;

        private void Awake()
        {
            SetModule(0);
        }

        public void OnClickNext()
        {
            SetModule(currModule + 1);
        }

        public void OnClickPrev()
        {
            SetModule(currModule - 1);
        }

        private bool SetModule(int moduleNum)
        {
            // Repeating so that we can always go next or prev
            moduleNum = (int)Mathf.Repeat(moduleNum, modules.Length);
            currModule = moduleNum;
            bool foundModule = false;
            // Enalbing module at moduleNum and disalbing all others
            for (int i = 0; i < modules.Length; i++)
            {
                if (i == moduleNum)
                {
                    foundModule = true;
                    modules[i].EnableModule(true);
                }
                else
                {
                    modules[i].EnableModule(false);
                }
            }
            return foundModule;
        }
    }
}