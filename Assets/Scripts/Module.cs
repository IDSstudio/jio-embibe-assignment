﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jio.Embibe.Assignment
{
    public class Module : MonoBehaviour
    {
        [SerializeField] private GameObject uiObject;

        /// <summary>
        /// Enable Module as well as its dependencies
        /// </summary>
        /// <param name="enable"></param>
        public void EnableModule(bool enable)
        {
            gameObject.SetActive(enable);
            if (uiObject != null)
            {
                uiObject.SetActive(enable);
            }
        }
    }
}