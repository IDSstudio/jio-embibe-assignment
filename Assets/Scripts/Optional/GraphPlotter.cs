﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jio.Embibe.Assignment.Optional
{
    [RequireComponent(typeof(LineRenderer))]
    public abstract class GraphPlotter : MonoBehaviour
    {
        private Camera cam;
        private LineRenderer graphLineRenderer;
        private Vector3[] currPoints;

        protected abstract int NumOfPoints { get; }
        protected abstract float GetYForX(float x);

        private void Awake()
        {
            cam = Camera.main;
            graphLineRenderer = GetComponent<LineRenderer>();
            currPoints = new Vector3[NumOfPoints];
            graphLineRenderer.positionCount = currPoints.Length;
        }

        /// <summary>
        /// Plot a graph with given number of points across Screen width
        /// </summary>
        protected void PlotGraph()
        {
            for (int i = 0; i < currPoints.Length; i++)
            {
                var currX = Screen.width * (float)i / (currPoints.Length - 1);
                var currY = GetYForX(currX);
                var worldPoint = cam.ScreenToWorldPoint(new Vector3(currX, currY, 10));
                currPoints[i] = worldPoint;
            }
            graphLineRenderer.SetPositions(currPoints);
        }
    }
}