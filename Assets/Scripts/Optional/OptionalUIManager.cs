﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Optional
{
    public class OptionalUIManager : MonoSingleton<OptionalUIManager>
    {
        public event System.Action<float> OnSlopeUpdate;
        public event System.Action<float> OnConstantUpdate;

        [SerializeField] private InputField slopeInput;
        [SerializeField] private InputField constantInput;

        protected override void InstanceSet()
        {
            slopeInput.characterValidation = InputField.CharacterValidation.Decimal;
            constantInput.characterValidation = InputField.CharacterValidation.Decimal;
            slopeInput.onValueChanged.AddListener(OnSlopeValueUpdated);
            constantInput.onValueChanged.AddListener(OnConstantValueUpdated);
        }

        public void UpdateSlopeConstantInputField(float slope, float constant)
        {
            slopeInput.text = slope.ToString();
            constantInput.text = constant.ToString();
        }

        private void OnSlopeValueUpdated(string newVal)
        {
            float.TryParse(newVal, out float slope);
            OnSlopeUpdate?.Invoke(slope);
        }

        private void OnConstantValueUpdated(string newVal)
        {
            float.TryParse(newVal, out float constant);
            OnConstantUpdate?.Invoke(constant);
        }
    }
}