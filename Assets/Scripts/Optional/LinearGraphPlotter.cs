﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Optional
{
    [RequireComponent(typeof(LineRenderer))]
    public class LinearGraphPlotter : GraphPlotter
    {
        private const int NUM_OF_POINTS = 2;

        private float currSlope;
        private float currConstant;

        protected override int NumOfPoints { get { return NUM_OF_POINTS; } }

        private void Start()
        {
            OptionalUIManager.Instance.OnSlopeUpdate += OnSlopeUpdate;
            OptionalUIManager.Instance.OnConstantUpdate += OnConstantUpdate;
            // Setting to default Values
            OptionalUIManager.Instance.UpdateSlopeConstantInputField(0.5f, 30);
        }

        private void OnSlopeUpdate(float val)
        {
            currSlope = val;
            PlotGraph();
        }

        private void OnConstantUpdate(float val)
        {
            currConstant = val;
            PlotGraph();
        }

        protected override float GetYForX(float x)
        {
            return currSlope * x + currConstant;
        }
    }
}