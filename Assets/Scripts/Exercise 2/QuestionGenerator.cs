﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Jio.Embibe.Assignment.Exercise2
{
    public class QuestionGenerator : MonoBehaviour
    {
        private void Start()
        {
            // Setting sample question for assigment
            // Later on, questions and answers can be dictated from JSON file with proper structure
            Exercise2UIManager.Instance.UpdateQuestionText("Which Exercise number is this?");
            Exercise2UIManager.Instance.UpdateAnswerButtons(new string[] { "4", "2", "3", "Optional" }, 1);
        }
    }
}