﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Jio.Embibe.Assignment.Exercise2
{
    public class Exercise2UIManager : MonoSingleton<Exercise2UIManager>
    {
        private const string ANIM_TRIGGER_CORRECT = "Correct";
        private const string ANIM_TRIGGER_INCORRECT = "Incorrect";

        private event System.Action<int> OnSelectOption;

        [SerializeField] private Text questionText;
        [SerializeField] private Button sampleAnswerButton;

        private Animator[] currAnswerButtons;
        private int correctIndex;

        protected override void InstanceSet()
        {

        }

        public void UpdateQuestionText(string question)
        {
            questionText.text = question;
        }

        public void UpdateAnswerButtons(IList<string> answers, int correctIndex)
        {
            this.correctIndex = correctIndex;
            if (currAnswerButtons != null)
            {
                // Destroying all answer buttons instances made previously
                // Better way could have been to use pooling
                for (int i = 0; i < currAnswerButtons.Length; i++)
                {
                    Destroy(currAnswerButtons[i].gameObject);
                }
            }
            sampleAnswerButton.gameObject.SetActive(false);
            currAnswerButtons = new Animator[answers.Count];
            // Creating instances of button using a sample,
            // so that variable amount of answers options can be present
            for (int i = 0; i < currAnswerButtons.Length; i++)
            {
                var button = Instantiate(sampleAnswerButton, sampleAnswerButton.transform.parent)
                    .GetComponent<Button>();
                button.gameObject.SetActive(true);
                var answerText = button.GetComponentInChildren<Text>();
                answerText.text = answers[i];
                currAnswerButtons[i] = button.GetComponent<Animator>();
                int index = i;
                button.onClick.AddListener(() => OnClickAnswer(index));
            }
        }

        private void OnClickAnswer(int index)
        {
            OnSelectOption?.Invoke(index);
            if (index == correctIndex)
            {
                currAnswerButtons[index].SetTrigger(ANIM_TRIGGER_CORRECT);
            }
            else
            {
                currAnswerButtons[index].SetTrigger(ANIM_TRIGGER_INCORRECT);
            }
        }
    }
}